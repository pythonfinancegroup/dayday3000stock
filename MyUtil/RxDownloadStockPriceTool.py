from typing import Dict, Any, List

from MyUtil.GetStockDataUtil import StockDataUtility
import datetime
import rx
from rx.scheduler import ThreadPoolScheduler
from rx import operators as ops
import investpy
import pandas as pd


class RxDownloadStockPriceTool:
    pool_scheduler = ThreadPoolScheduler(10)

    def _create_stock_inf_o(self, name: str):
        su = StockDataUtility()
        start_date = datetime.datetime(1800, 5, 12, 0, 0, 0, tzinfo=datetime.timezone.utc)
        end_date = datetime.datetime.today()
        try:
            # end_date = datetime.datetime(2020, 5, 13, 0, 0, 0)
            startDateStr = start_date.strftime('%d/%m/%Y')
            endDateStr = end_date.strftime('%d/%m/%Y')
            country = 'United States'

            # df = investpy.get_stock_historical_data(stock=name,
            #                                         country=country,
            #                                         from_date=startDateStr,
            #                                         to_date=endDateStr)

            df = su.get_a_stock_raw_data_from_investing(start_date, end_date, name, 'united states')
            d_df: Dict[str, pd.DataFrame] = {name: df}
            print('{} get price success from investing.com '.format(name))
            return rx.of(d_df)
        except Exception as e:
            print('{} get price fail at investing.com:{} '.format(name, e))
            try:
                print('{} get price try get price from yahoo '.format(name))
                df = su.get_stock_price_from_yahoo(start_date, end_date, name)
                d_df: Dict[str, pd.DataFrame] = {name: df}
                return rx.of(d_df)
            except:
                print('{} get price fail :{} '.format(name, e))
                return rx.of({name: None})

    def _create_stock_obs(self, name: str):
        stock_o = rx.defer(lambda s: self._create_stock_inf_o(name)).pipe(
            ops.subscribe_on(self.pool_scheduler)
        )
        return stock_o

    def downloadUs_index_stock_price(self):
        su_global = StockDataUtility()
        print('start get the stock s name')
        stocks = su_global.get_sp500_NASDAQ_100_dji_30_stock()
        # stocks = ['rtx']
        print('finish get the stock s name')
        stocks_obs = []
        for stock in stocks:
            stocks_obs.append(self._create_stock_obs(stock))

        print('start get price at {}'.format(datetime.datetime.today()))
        return rx.combine_latest(*stocks_obs)
