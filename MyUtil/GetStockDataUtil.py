from typing import List, Any, Union

import investpy
from datetime import date
import pandas as pd
import sqlite3
from bs4 import BeautifulSoup
import requests
import re
import json
import datetime
import numpy as np
import io

from tqdm import tqdm


class StockDataUtility:
    rename_columns = {
        'Open': 'open'.lower(),
        'High': 'High'.lower(),
        'Low': 'Low'.lower(),
        'Close': 'Close'.lower(),
        'Adj Close': 'Adj_Close'.lower(),
        'Volume': 'Volume'.lower(),
        'Currency': 'Currency'.lower()
    }

    def __init__(self):
        self.connect = sqlite3.connect('StockDataUtility.sqlite3')

    def show_stock_country_list(self):
        print(investpy.get_stock_countries())

    def create_table_name(self, stock_name: str, country: str) -> str:
        table_name = stock_name + '_' + country.replace(' ', '_')
        return table_name

    def get_a_stock_raw_data_from_investing(self, startDate: date, endDate: date, name: str,
                                            country: str) -> pd.DataFrame:
        startDateStr = startDate.strftime('%d/%m/%Y')
        endDateStr = endDate.strftime('%d/%m/%Y')

        df = investpy.get_stock_historical_data(stock=name,
                                                country=country,
                                                from_date=startDateStr,
                                                to_date=endDateStr)

        df.rename(columns=self.rename_columns, inplace=True)

        return df

    # def save_df_to_sqlite(self, table_name: str, df: pd.DataFrame):
    #     df.to_sql(table_name, self.connect, if_exists='replace')

    # def read_sqlite_to_df(self, table_name: str):
    #     readed_df = pd.read_sql_query('select * from ' + table_name, self.connect, parse_dates=['Date'],
    #                                   index_col=['Date'])
    #
    #     return readed_df

    def get_stock_pbr(self, name: str, country: str):
        try:
            profile_json = investpy.get_stock_company_profile(stock=name, country=country, language='english')
            headers = {
                'User-Agent': 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6'
            }
            financial_ratios_url = profile_json['url'].replace('company-profile', 'ratios')
            resp = requests.get(financial_ratios_url, headers=headers)
            resp.encoding = 'utf-8'
            web_content = resp.text + ""
            soup = BeautifulSoup(web_content, 'html.parser')
            pb_tag = soup.find_all(text='Price to Book ')
            pbr = float(pb_tag[0].parent.parent.parent.find_all('td')[1].text)
            return pbr
        except (ValueError, RuntimeError) as e:
            return None

    def get_us_stock_pbs(self, stock: str):
        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6'
        }

        link = 'https://www.macrotrends.net/assets/php/fundamental_iframe.php?t={name}&type=price-book&statement=price-ratios&freq=Q'.format(
            name=stock)
        resp = requests.get(link, headers=headers)
        resp.encoding = 'utf-8'
        web_content = resp.text
        # print(web_content)
        result = re.findall('var chartData =[^\]]*\]', web_content)
        # print(result[0])
        data = result[0].split('=')[1]
        data_array = json.loads(data)
        # v3 is pb ratio
        date_index = []
        pb_data = []
        for data in data_array:
            date = datetime.datetime.strptime(data['date'], '%Y-%m-%d')
            pb = data['v3']
            date_index.append(date)
            pb_data.append(pb)

        pb_se = pd.Series(pb_data, date_index)
        return pb_se

    def get_dji_30_stock_name(self) -> List[str]:
        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6'
        }
        link = 'https://en.wikipedia.org/wiki/Dow_Jones_Industrial_Average'
        resp = requests.get(link, headers=headers)
        resp.encoding = 'utf-8'
        web_content = resp.text
        # print(web_content)
        soup = BeautifulSoup(web_content, 'html.parser')
        table = soup.find(id='constituents')
        a_s = table.find_all('a', {'class': 'external text'})
        dji = []
        for a in a_s:
            dji.append(a.text)
        return dji

    def get_NASDAQ_00_stock_name(self) -> List[str]:
        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6'
        }
        link = 'https://en.wikipedia.org/wiki/NASDAQ-100'
        resp = requests.get(link, headers=headers)
        resp.encoding = 'utf-8'
        web_content = resp.text
        # print(web_content)
        soup = BeautifulSoup(web_content, 'html.parser')
        table = soup.find(id='constituents')
        a_s = table.find_all('a')
        stock_names = []
        for a in a_s:
            stock_names.append(a.parent.findNext('td').text.replace('\n', ''))

        return stock_names

    def get_sp500_stock_name(self) -> List[str]:
        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6'
        }
        link = 'https://en.wikipedia.org/wiki/List_of_S%26P_500_companies'
        resp = requests.get(link, headers=headers)
        resp.encoding = 'utf-8'
        web_content = resp.text
        # print(web_content)
        soup = BeautifulSoup(web_content, 'html.parser')
        table = soup.find(id='constituents')
        a_s = table.find_all('a', attrs={'rel': 'nofollow',
                                         'class': 'external text',
                                         },
                             )
        result = []
        for a in a_s:
            text = a.text
            if text != 'reports':
                result.append(text)

        return result

    def get_sp500_stock_name_2(self) -> List[str]:
        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6'
        }
        link = 'https://www.cnyes.com/usastock/hotprice.aspx?page=hot&kind=sp500'
        resp = requests.get(link, headers=headers)
        resp.encoding = 'utf-8'
        web_content = resp.text
        # print(web_content)
        soup = BeautifulSoup(web_content, 'html.parser')
        table = soup.find(id='ctl05_TBstock')
        trs = table.find_all('tr')
        stock_names = []
        for tr in trs:
            tds = tr.find_all('td')

            if len(tds) > 0:
                a = tds[1].select('a')
                if a is not None:
                    stock_name = a[0].text
                    stock_names.append(stock_name)
        return stock_names

    def get_sp500_NASDAQ_100_dji_30_stock(self):
        sp500 = self.get_sp500_stock_name_2()
        n100 = self.get_NASDAQ_00_stock_name()
        dji = self.get_dji_30_stock_name()
        sum_stocks = sp500 + n100 + dji
        hashmap = {}

        for stock in sum_stocks:
            hashmap[stock] = stock
        result = list(hashmap.values())
        result = result + ['voo', 'qqq', 'dia', 'iwm', 'spsm', 'vywo', 'HSI']
        result.sort()
        return result

    def get_stock_price_from_yahoo(self, start_date: datetime.datetime, end_date: datetime.datetime, name: str):
        period1 = int(start_date.timestamp())
        period2 = int(end_date.timestamp())

        link = 'https://query1.finance.yahoo.com/v7/finance/download/{name}?period1={p1}&period2={p2}&interval=1d&events=history'.format(
            name=name, p1=period1, p2=period2)
        r = requests.get(link)
        data = r.content.decode('utf8')
        df = pd.read_csv(io.StringIO(data), parse_dates=['Date'], index_col='Date')
        df.rename(columns=self.rename_columns, inplace=True)
        return df
