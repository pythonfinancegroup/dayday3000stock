import sqlite3
from typing import Optional

import pandas as pd
import datetime


class StockLocalData:
    # rename_columns = {
    #     'Open': 'open'.lower(),
    #     'High': 'High'.lower(),
    #     'Low': 'Low'.lower(),
    #     'Close': 'Close'.lower(),
    #     'Adj Close': 'Adj_Close'.lower(),
    #     'Volume': 'Volume'.lower(),
    #     'Currency': 'Currency'.lower()
    # }

    must_contain_column = [
        'open',
        'high',
        'low',
        'close',
        'volume'

    ]

    def __init__(self):
        self.connect = sqlite3.connect('StockDataUtility.sqlite3')

    def read_stock_all_data(self, table_name: str):
        try:
            readed_df = pd.read_sql_query('select * from ' + table_name, self.connect, parse_dates=['Date'],
                                          index_col=['Date'])

            return readed_df
        except:
            print("{} data not found".format(table_name))
            return None

    def read_stock_data_date(self, table_name: str, date: datetime.datetime):
        try:
            readed_df = pd.read_sql_query('select * from ' + table_name, self.connect, parse_dates=['Date'],
                                          index_col=['Date'])

            return readed_df.loc[date]
        except:
            print("{} data not found".format(table_name))
            return None

    def read_stock_data_range(self, table_name: str, start_date: Optional[datetime.datetime],
                              end_date: Optional[datetime.datetime]):
        try:
            readed_df = pd.read_sql_query('select * from ' + table_name, self.connect, parse_dates=['Date'],
                                          index_col=['Date'])

            return readed_df.loc[start_date:end_date]
        except:
            print("{} data not found".format(table_name))
            return None

    def save_df_to_sqlite(self, table_name: str, df: pd.DataFrame):
        # TODO check the DataFrame
        contain_columns = df.columns
        for c in self.must_contain_column:
            if c not in contain_columns:
                raise Exception('do not have column: {}'.format(c))

        df.to_sql(table_name, self.connect, if_exists='replace')
