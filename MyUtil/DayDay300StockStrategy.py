import datetime
from enum import Enum
from typing import Optional, List

import pandas as pd
import ffn
import talib


class SmallMarketCrash:
    enter_crash_date: datetime.datetime
    enter_crash_price: float
    out_date: datetime.datetime
    out_price: float
    high_ref_price_date: datetime.datetime
    high_ref_price: float
    current_lower_price_date: datetime.datetime
    current_lower_price: float
    crash_finish: bool


class DayDay3000StockStrategy:
    _must_contain_column = [
        'open',
        'high',
        'low',
        'close',
        'volume'

    ]

    def __init__(self, df: pd.DataFrame) -> None:
        super().__init__()
        self.df = df

    def is_a_suitable_df_data(self):
        contain_columns = self.df.columns
        for c in self._must_contain_column:
            if c not in contain_columns:
                return False
        return True

    def is_larger_equal_5year_data(self):
        most_old_date = pd.to_datetime(self.df.index.values[0])
        most_new_date = pd.to_datetime(self.df.index.values[-1])
        # print(type(most_old_date))
        d = most_new_date - most_old_date
        d_days = d.days
        five_year_day = 365 * 5
        return d_days >= five_year_day

    def calculate_max_draw_down(self) -> float:
        close_df = self.df['close']
        return ffn.core.calc_max_drawdown(close_df)

    def is_a_up_trend_stock(self, pass_fun=0.75):
        ma = talib.SMA(self.df['close'], timeperiod=365)
        ma_diff = ma.diff()
        up_df = ma_diff.dropna() >= 0
        up_list = up_df.to_list()
        length = len(up_list)
        up_sum = 0
        for up in up_list:
            if up:
                up_sum = up_sum + 1
        fun = 0
        if length != 0:
            fun = up_sum / length
        # print(fun)

        return fun > pass_fun

    def find_all_the_small_market_crash(self):
        small_market_crashes: List[SmallMarketCrash] = []
        for start_i in range(0, len(self.df)):
            may_be_high_date = self.df.iloc[start_i].name.to_pydatetime()
            is_the_may_be_high_date_already_in_a_crash_period = self._date_is_within_the_mall_crash_array(
                may_be_high_date, small_market_crashes)
            if is_the_may_be_high_date_already_in_a_crash_period:
                continue

            temp_high_price = self.df.loc[may_be_high_date]['high']
            temp_low_price = None
            temp_low_price_date = None
            market_crash_date = None
            is_in_market_crash = False
            now_out_price = 0
            for end_i in range(start_i, len(self.df)):
                date = self.df.iloc[end_i].name.to_pydatetime()
                # print("h: {} c: {} ".format(may_be_high_date, date))
                is_check_ing_last_day = len(self.df) - end_i == 1
                if not is_in_market_crash:
                    is_this_a_high_price_market_crash_date_pair: Check_high_price_market_crash_date_pair_Response = self.is_this_a_high_price_market_crash_date_pair(
                        may_be_high_date, date)

                    if is_this_a_high_price_market_crash_date_pair == Check_high_price_market_crash_date_pair_Response.NOT_BUT_CAN_CONTINUE_CHECK:
                        continue

                    if is_this_a_high_price_market_crash_date_pair == Check_high_price_market_crash_date_pair_Response.NOT_AND_STOP_CHECK_THIS_HIGH_PRICE_DATE:
                        break

                    if is_this_a_high_price_market_crash_date_pair == Check_high_price_market_crash_date_pair_Response.YES:
                        is_in_market_crash = True
                        market_crash_date = date

                # now is in the small market crash
                high = self.df.loc[date]['high']
                low = self.df.loc[date]['low']
                if (high > temp_high_price) | (low > temp_high_price):
                    break
                if (temp_low_price is None) or (low < temp_low_price):
                    temp_low_price = low
                    temp_low_price_date = date

                now_out_price = (temp_high_price - temp_low_price) / 2 + temp_low_price

                if high >= now_out_price:
                    # reach the out price
                    smc = SmallMarketCrash()
                    smc.out_price = now_out_price
                    smc.current_lower_price = temp_low_price
                    smc.high_ref_price = temp_high_price
                    smc.enter_crash_price = temp_high_price * 0.8
                    smc.out_date = date
                    smc.high_ref_price_date = may_be_high_date
                    smc.current_lower_price_date = temp_low_price_date
                    smc.enter_crash_date = market_crash_date
                    smc.crash_finish = True
                    small_market_crashes.append(smc)
                    break

                if is_check_ing_last_day:
                    smc = SmallMarketCrash()
                    smc.out_date = date
                    smc.out_price = now_out_price
                    smc.high_ref_price_date = may_be_high_date
                    smc.high_ref_price = temp_high_price
                    smc.enter_crash_price = temp_high_price * 0.8
                    smc.current_lower_price_date = temp_low_price_date
                    smc.current_lower_price = temp_low_price
                    smc.enter_crash_date = market_crash_date
                    smc.crash_finish = False
                    small_market_crashes.append(smc)

                # not yet reach the out price

                # print("h: {} c: {} hp:{}".format(may_be_high_date, date, temp_high_price))

        # after enter the market crash mode
        # the same of next date can be 3 type:
        # 1.lower price
        # 2.out price
        # 3.Nothing

        return small_market_crashes

    def _date_is_within_the_mall_crash_array(self, date: datetime.datetime,
                                             small_market_crashes: List[SmallMarketCrash]):
        for crash in small_market_crashes:
            if (date >= crash.high_ref_price_date) & (date <= crash.out_date):
                return True

        return False

    def is_this_a_high_price_market_crash_date_pair(self, high_price_date: datetime.datetime,
                                                    market_crash_date: datetime.datetime, market_crash_condition=0.8):
        try:
            high_price = self.df.loc[high_price_date]['high']
        except:
            high_price = None
        try:
            market_crash_price = self.df.loc[market_crash_date]['low']
        except:
            market_crash_price = None
        if (high_price is None):
            # print('1')
            return Check_high_price_market_crash_date_pair_Response.NOT_AND_STOP_CHECK_THIS_HIGH_PRICE_DATE

        if (market_crash_price is None):
            # print('1')
            return Check_high_price_market_crash_date_pair_Response.NOT_BUT_CAN_CONTINUE_CHECK

        # high_price_date_add_1 = high_price_date + datetime.timedelta(days=1)
        df_high_price_market_crash_date = self.df.loc[high_price_date:market_crash_date]
        filtered_df = df_high_price_market_crash_date[df_high_price_market_crash_date['high'] > high_price]
        if len(filtered_df):
            # has price > high_price between the dates
            # print('2')
            return Check_high_price_market_crash_date_pair_Response.NOT_AND_STOP_CHECK_THIS_HIGH_PRICE_DATE

        real_market_crash_price = high_price * market_crash_condition
        if market_crash_price > real_market_crash_price:
            # print('3')
            # print(market_crash_price)
            # print(real_market_crash_price)
            return Check_high_price_market_crash_date_pair_Response.NOT_BUT_CAN_CONTINUE_CHECK

        return Check_high_price_market_crash_date_pair_Response.YES


class Check_high_price_market_crash_date_pair_Response(Enum):
    NOT_BUT_CAN_CONTINUE_CHECK = 'NOT_BUT_CAN_CONTINUE_CHECK'
    YES = 'YES'
    NOT_AND_STOP_CHECK_THIS_HIGH_PRICE_DATE = 'NOT_AND_STOP_CHECK_THIS_HIGH_PRICE_DATE'

# select stock
#     1. stock in the index or ETF
#     2. up trend
#     3. >= 5  years data
#     more
#     4. different industry

#     buy stock
#         average the money
#     in market   10下10下
#     out market
#       earn
#           1.有賺即閃, MDD >= 80 %,must use this
#           2.放長線釣大魚
#
#       loss
#            3 year
