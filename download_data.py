from pandas import DataFrame

from MyUtil.GetStockDataUtil import StockDataUtility
from MyUtil.RxDownloadStockPriceTool import RxDownloadStockPriceTool
from typing import Dict, Any, List
import pandas as pd
import datetime

from MyUtil.StockLocalData import StockLocalData
from find_down_20_stocks import *


def display_combine_latest_result(rs: List[Dict[str, pd.DataFrame]]):
    print('finish get price at {}'.format(datetime.datetime.today()))
    sdu = StockDataUtility()
    sld = StockLocalData()

    for r in rs:
        for key in r.keys():
            df: DataFrame = r[key]
            if df is not None:
                # print(df)
                sld.save_df_to_sqlite(key.lower(), df)

    find_down_20_run()


rxD = RxDownloadStockPriceTool()
rxD.downloadUs_index_stock_price().subscribe(
    on_next=display_combine_latest_result
)
