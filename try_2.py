import datetime
import json

from MyUtil.DayDay300StockStrategy import DayDay3000StockStrategy
from MyUtil.GetStockDataUtil import StockDataUtility
from MyUtil.StockLocalData import StockLocalData

su = StockDataUtility()
stocks = su.get_sp500_NASDAQ_100_dji_30_stock()
local_data = StockLocalData()
mmm_df = local_data.read_stock_all_data('fb')
# print(mmm_df)

dd3000 = DayDay3000StockStrategy(mmm_df)
# high_price = dd3000.get_ref_high_price(138.69, datetime.datetime(2020, 5, 17))
rs = dd3000.find_all_the_small_market_crash()
for r in rs:
    high_ref_price_date = r.high_ref_price_date
    enter_crash_date = r.enter_crash_date
    current_lower_price_date = r.current_lower_price_date
    out_date = r.out_date

    high_price = r.high_ref_price
    enter_crash_price = r.enter_crash_price
    current_lower_price = r.current_lower_price
    out_price = r.out_price

    print(
        "high_ref_price_date: {high_ref_price_date} ${high_price} enter_crash_date:{enter_crash_date} ${"
        "enter_crash_price} current_lower_price_date:{current_lower_price_date} ${current_lower_price} out_date:{"
        "out_date} ${out_price}".format(
            high_ref_price_date=high_ref_price_date, high_price=high_price, enter_crash_date=enter_crash_date,
            enter_crash_price=enter_crash_price, current_lower_price_date=current_lower_price_date,
            current_lower_price=current_lower_price, out_date=out_date, out_price=out_price
        ))
