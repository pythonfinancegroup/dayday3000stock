import datetime

from tqdm import tqdm

from MyUtil.DayDay300StockStrategy import DayDay3000StockStrategy
from MyUtil.GetStockDataUtil import StockDataUtility
from MyUtil.StockLocalData import StockLocalData


def find_down_20_run():
    su = StockDataUtility()
    stocks = su.get_sp500_NASDAQ_100_dji_30_stock()
    # stocks = ['mmm']
    local_data = StockLocalData()

    good_stock = {}
    for stock in tqdm(stocks):
        df = local_data.read_stock_all_data(stock)
        if df is not None:
            dd3000 = DayDay3000StockStrategy(df)
            if dd3000.is_a_suitable_df_data() and dd3000.is_larger_equal_5year_data() and (abs(
                    dd3000.calculate_max_draw_down()) < 0.8) and dd3000.is_a_up_trend_stock():
                market_crashes = dd3000.find_all_the_small_market_crash()
                if len(market_crashes) > 0:
                    last_crash = market_crashes[-1]
                    if not last_crash.crash_finish:
                        good_stock[stock] = last_crash

    for key in good_stock:
        print(key)
        r = good_stock[key]
        high_ref_price_date = r.high_ref_price_date
        enter_crash_date = r.enter_crash_date
        current_lower_price_date = r.current_lower_price_date
        out_date = r.out_date

        high_price = r.high_ref_price
        enter_crash_price = r.enter_crash_price
        current_lower_price = r.current_lower_price
        out_price = r.out_price

        print(
            "high_ref_price_date: {high_ref_price_date} ${high_price} enter_crash_date:{enter_crash_date} ${"
            "enter_crash_price} current_lower_price_date:{current_lower_price_date} ${current_lower_price} out_date:{"
            "out_date} ${out_price}".format(
                high_ref_price_date=high_ref_price_date, high_price=high_price, enter_crash_date=enter_crash_date,
                enter_crash_price=enter_crash_price, current_lower_price_date=current_lower_price_date,
                current_lower_price=current_lower_price, out_date=out_date, out_price=out_price
            ))

# find_down_20_run()
