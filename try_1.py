import datetime
import pandas as pd
import ffn

from MyUtil.DayDay300StockStrategy import DayDay3000StockStrategy
from MyUtil.GetStockDataUtil import StockDataUtility
from MyUtil.StockLocalData import StockLocalData

su = StockDataUtility()
sld = StockLocalData()

# df = sld.read_stock_all_data('mmm')
df = sld.read_stock_data_range('mmm', None, None)
dd_s = DayDay3000StockStrategy(df)

print(dd_s.is_larger_equal_5year_data())
print(dd_s.calculate_max_draw_down())
